<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



/*

*

* @author 	    Brayan Acebo

* @package 	PyroCMS

* @subpackage 	Products

* @category 	Modulos

*/



class Our_Team extends Public_Controller {



    public function __construct() {

        parent::__construct();

        $this->load->model('team_m');

    }





// -----------------------------------------------------------------



     public function index($lang="")

    {

          if(empty($lang)){



            $this->db->from($this->db->dbprefix('team'));

            $query = $this->db->get();

            $solucion = $query->result();

            $projects = (array) $solucion;

            

            $_SESSION['lang']="es";

            $_SESSION['controlador']="team";



            $this->lang->load('team','spanish');



        }else if($lang=="es"){



            $this->db->from($this->db->dbprefix('team'));

            $query = $this->db->get();

            $solucion = $query->result();

            $projects = (array) $solucion;



            $_SESSION['lang']="es";

            $_SESSION['controlador']="team";



            $this->lang->load('team','spanish');



        }

         $team = $this->team_m->get_all();

        $data['team'] = $projects;

   

    	// Banner

       



        $pagination = create_pagination('our_team/index', $this->team_m->count_all(), 6, 3);



        $team = $this->team_m

        ->limit($pagination['limit'], $pagination['offset'])

        ->order_by('position','ASC')

        ->get_all();



        foreach($team AS $item)

        {

            $item->name = substr($item->name, 0, 34);

            $item->cargo = substr($item->cargo, 0, 34);

            $item->image = val_image($item->image);

            $item->exp = substr($item->exp, 0, 200);
              $item->fun = substr($item->fun, 0, 200);


      

            

        }



        $this->template

        ->set('team', $team)

        ->set('pagination', $pagination['links'])

        ->build('index');

    }



    // ----------------------------------------------------------------------



    

}