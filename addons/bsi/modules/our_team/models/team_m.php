<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



/**

 * @author  Brayan Acebo

 */

class Team_m extends MY_Model {



    public function __construct()

    {

        parent::__construct();

        $this->_table = $this->db->dbprefix . 'team';

    }

    

         function new_record($archivos) {





        $config = array(

            'allowed_types' => 'pdf|jpg|png|gif',

            'upload_path' => $this->gallery_path,

            'max_size' => 0

        );



        $this->load->library('Upload', $config);



        if (!$this->upload->do_upload()) {

            

        } else {

            $data = array('upload_data' => $this->upload->data());

            if (!empty($data['upload_data']['file_name'])) {

                $name = $data['upload_data']['file_name'];

                $archivos['imagen'] = $name;

                $this->db->insert($this->db->dbprefix('team'), $archivos);

            } else {

                $this->db->insert($this->db->dbprefix('team'), $archivos);

            }



            return true;

        }

    }



    function update_record($archivos, $id) {



        $config = array(

            'allowed_types' => 'pdf|jpg|png|gif',

            'upload_path' => $this->gallery_path,

            'max_size' => 0

        );



        $this->load->library('Upload', $config);



        if (!$this->upload->do_upload()) {

            $this->db->where('id', $id);

            $this->db->update($this->db->dbprefix('team'), $archivos);

        } else {

            $data = array('upload_data' => $this->upload->data());

            if (!empty($data['upload_data']['file_name'])) {

                $name = $data['upload_data']['file_name'];

                $archivos['imagen'] = $name;

                $this->db->where('id', $id);

                $this->db->update($this->db->dbprefix('team'), $archivos);

            } else {

                $this->db->where('id', $id);

                $this->db->update($this->db->dbprefix('team'), $archivos);

            }



            return true;

        }

    }

    

            function update_estado($archivos,$id){

        

        $this->db->where('id',$id);

        $this->db->update($this->db->dbprefix('team'),$archivos);

        

    }



  /*   function update_estado2($archivos,$id){

        

        $this->db->where('id',$id);

        $this->db->update($this->db->dbprefix('product'),$archivos);

        

    }*/

}