<?php



defined('BASEPATH') or exit('No direct script access allowed');



class Module_Our_Team extends Module {



    public $version = '1.3';



    public function info() {

        return array(

            'name' => array(

                'en' => 'Nuestro Equipo',

                'es' => 'Nuestro Equipo'

            ),

            'description' => array(

                'en' => 'Nuestro Equipo © Juan Zapata',

                'en' => 'Our Team © Juan Zapata',

            ),

            'frontend' => TRUE,

            'backend' => TRUE,

            'menu' => 'content'

        );

    }



    public function install() {



        /* Creación del directorio para carga de imagenes */

        $this->dbforge->drop_table('team');



        $team = array(

            'id' => array(

                'type' => 'INT',

                'constraint' => '11',

                'auto_increment' => true

            ),

            'name' => array(

                'type' => 'VARCHAR',

                'constraint' => '255',

                'null' => false

            ),

             'cargo' => array(

                'type' => 'VARCHAR',

                'constraint' => '255',

                'null' => true

            ),

            'image' => array(

                'type' => 'VARCHAR',

                'constraint' => '455',

                'null' => true

            ),
 'exp' => array(

                 'type' => 'TEXT',                

                 'null' => true),
           

            

                 'fun' => array('type' => 'TEXT', 'null' => true            

                     ),

           


             'position' => array(

                 'type' => 'INT', 

                 'constraint' => '11', 

                 'null' => true),

                         );

              

     

        $this->dbforge->add_field($team);

        $this->dbforge->add_key('id', true);



         if ($this->dbforge->create_table('team') AND

                is_dir($this->upload_path . 'team') OR @mkdir($this->upload_path . 'team', 7777, TRUE)) {

           
             return TRUE;

        }

      

        }





    public function uninstall() {

        $this->dbforge->drop_table($this->team);



      

        return true;

    }



    public function upgrade($old_version) {

        return true;

    }



    public function help() {

        return "Modulo de Nuestro Equipo";

    }



}



/* Fin del archivo details.php */