<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Christian España
 */
class Admin extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->lang->load('language');
        $this->load->library('form_validation');
        $this->template
             ->append_js('module::developer.js')
             ->append_metadata($this->load->view('fragments/wysiwyg', null, TRUE));
        $this->load->model(array('contact_us_m', 'contact_us_emails_m'));
    }

    /**
     * List all domains
     */
    public function index() {
        $result = $this->contact_us_m->get_all();
		
        $funcion = 'create';
        $post = array();
        if (count($result) > 0) {
            $funcion = 'edit';
            $post = $result[0];
        }
		
		$pagination = create_pagination('admin/contact_us/index/', $this->contact_us_emails_m->count_all(), 10);
		$contact_us_emails = $this->contact_us_emails_m->limit($pagination['limit'], $pagination['offset'])->get_all();
		
        $this->template->set('funcion', $funcion)
                ->set('data', $post)
				->set('data2', $contact_us_emails)
				->set('pagination', $pagination)
                ->build('admin/contact_us_back');
    }

    public function create() {
       $data = array(
            'facebook' => $this->input->post('facebook'),
            'twitter' => $this->input->post('twitter'),
            'linkedin' => $this->input->post('linkedin'),
            'adress' => $this->input->post('adress'),
            'phone' => $this->input->post('phone'),
            'schedule' => html_entity_decode($this->input->post('text')),
            'email' => $this->input->post('email'),
            'map' =>  html_entity_decode($this->input->post('map'))
        );

        if ($this->contact_us_m->insert($data)) {
            // insert ok, so
            $this->session->set_flashdata('success', lang('contact_us:success_message'));
            redirect('admin/contact_us/');
        } else {
            $this->session->set_flashdata('error', lang('contact_us:error_message'));
            redirect('admin/contact_us/');
        }

        $this->template->set('funcion', 'create')
                ->build('admin/contact_us_back');
    }

    public function edit()
    {
        $data = array(
            'adress' => $this->input->post('adress'),
            'phone' => $this->input->post('phone'),
            'schedule' => html_entity_decode($this->input->post('text')),
            'email' => $this->input->post('email'),
            'map' =>  html_entity_decode($this->input->post('map'))
        );
		
        if ($this->contact_us_m->update_all($data)) {

            // insert ok, so
            $this->session->set_flashdata('success', lang('contact_us:success_message'));
            redirect('admin/contact_us/');
        } else {
            $this->session->set_flashdata('error', lang('contact_us:error_message'));
            redirect('admin/contact_us/');
        }

        $this->template->set('funcion', 'edit')
                ->build('admin/contact_us_back');
    }

}