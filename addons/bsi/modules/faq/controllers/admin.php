<?php

defined('BASEPATH') OR exit('No direct script access allowed');



/**

 * @author Brayan Acebo

 */



// Ajustamos Zona Horaria

date_default_timezone_set("America/Bogota");



class Admin extends Admin_Controller {



    public function __construct()

	{

		parent::__construct();

		$this->lang->load('team');

		$this->template

		->append_js('module::developer.js')

		->append_metadata($this->load->view('fragments/wysiwyg', compact('items'), TRUE));

		$this->load->model('faq_m');

	

                  $this->db->from($this->db->dbprefix('faq'));

              

                $query = $this->db->get();

                $solucion = $query->result();

                $this->result = (object) $solucion;

	}



        



    // -----------------------------------------------------------------



    public function index() {

     

        $this->db->from($this->db->dbprefix('faq'));

        $query1 = $this->db->get();

        $solucion1 = $query1->result();

       // $this->result = (object) $solucion;



        $count = count($solucion1);



        $data['count'] = $count;

        $data['faqs'] = $this->result;

		// products

		$pagination = create_pagination('admin/faq/index', $this->faq_m->count_all(), 10);



		$faq = $this->faq_m

                ->order_by('id', 'DESC')

                ->limit($pagination['limit'], $pagination['offset'])

                ->get_all();



        // Consultamos las categorias

       $pag = $pagination['offset'];



		$this->template

		->set('faqs', $faq)

		->set('pagination', $pagination)

		->set('pag', $pag)

		->build('admin/index',$data);

	}

        

        

        

        
        

   /*public function estado2($estado2,$id){



        $this->load->model('product_model');

        $this->db->select('estado2');

        $this->db->where('estado2', 1);

        $this->db->from($this->db->dbprefix('product'));

        $query = $this->db->get();

        $solucion = $query->result();

       // $this->result = (object) $solucion;



        $count = count($solucion);

        if($count < 3){

        $archivos = array('estado2'=> $estado2 );

        

        $this->product_model->update_estado2($archivos,$id);

        redirect('admin/products');

        }else{

        $archivos = array(

       'estado'=> 0        

        );

        $this->product_model->update_estado2($archivos,$id);

        redirect('admin/products');

        }



    }   */  

        



	public function edit_faq($idItem = null)

	{

		$this->form_validation->set_rules('pregunta', 'Pregunta', 'required|trim');

                $this->form_validation->set_rules('respuesta', 'Respuesta', 'required|trim');

              


		

		if($this->form_validation->run()!==TRUE)  // abrimos el formulario de edicion

		{

			if(validation_errors() == "")

			{

				$this->session->set_flashdata('error', validation_errors());

			}

			if(!empty($idItem))  // si se envia un dato por la URL se hace lo siguiente (Edita)

			{

				$idItem or redirect('admin/faq');



				$titulo = 'Editar ';

				$datosForm = $this->faq_m->get($idItem);



				$positionFaqs = $this->faq_m

				->order_by('position', 'ASC')

				->get_all();



				$this->template

				->set('datosForm', $datosForm)

                ->set('positionFaqs', $positionFaqs)

				->set('titulo', $titulo)

				->set('ban', true)

				->build('admin/edit');

			}

			else

			{

				$titulo = 'Crear ';



				$this->template

				->set('titulo', $titulo)

				->set('ban', false)

				->build('admin/edit');

			}

		}

		else // si el formulario ha sido enviado con éxito se procede

		{

			if($idItem != null)  // si se envia un dato por la URL se hace lo siguiente (Edita)

			{



				$post = (object) $this->input->post();

				



				$data = array(

					'pregunta' => $post->pregunta,

                   'respuesta' => $post->respuesta,

                  


					

					);



				$config['upload_path'] = './' . UPLOAD_PATH . '/faq';

				$config['allowed_types'] = 'gif|jpg|png|jpeg';

				$config['max_size'] = 2050;

				$config['encrypt_name'] = true;



				$this->load->library('upload', $config);



	            // imagen uno

				//$img = $_FILES['image']['name'];



				if (!empty($img)) {

					if ($this->upload->do_upload('image')) {

						$datos = array('upload_data' => $this->upload->data());

						$path = UPLOAD_PATH . 'faq/' . $datos['upload_data']['file_name'];

						$img = array('image' => $path);

						$data = array_merge($data, $img);

						$obj = $this->db->where('id', $idItem)->get('faq')->row();

						@unlink($obj->image);

					} else {

						$this->session->set_flashdata('error', $this->upload->display_errors());

						redirect('admin/faq/');

					}

				}

				if ($this->faq_m->update($idItem, $data)) {

					// Se actualiza el Orden

					



					$this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');

					redirect('admin/faq/');

				} else {

					$this->session->set_flashdata('success', lang('home:error_message'));

					redirect('admin/faq/');

				}

			}

			else

			{

				$post = (object) $this->input->post();



				$this->db->select_max('position');

				$query = $this->db->get('faq');

				$position = $query->row();



				$data = array(

					
					'pregunta' => $post->pregunta,

                   'respuesta' => $post->respuesta,

                 
                                   

					

					);



				$config['upload_path'] = './' . UPLOAD_PATH . '/home_banner';

				$config['allowed_types'] = 'gif|jpg|png|jpeg';

				$config['max_size'] = 2050;

				$config['encrypt_name'] = true;



				$this->load->library('upload', $config);



	            // imagen uno

				//$img = $_FILES['image']['name'];



				if (!empty($img)) {

					if ($this->upload->do_upload('image')) {

						$datos = array('upload_data' => $this->upload->data());

						$path = UPLOAD_PATH . 'home_banner/' . $datos['upload_data']['file_name'];

						$img = array('image' => $path);

						$data = array_merge($data, $img);

					} else {

						$this->session->set_flashdata('error', $this->upload->display_errors());

						redirect('admin/faq/');

					}

				}



				if ($this->faq_m->insert($data)) {

					$this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');

				} else {

					$this->session->set_flashdata('success', lang('home:error_message'));

				}

				redirect('admin/faq');

			}

		}



	}



	public function delete_faq($id = null) {



		$id or redirect('admin/faq');



		$obj = $this->db->where('id', $id)->get($this->db->dbprefix.'faq')->row();



		if ($this->faq_m->delete($id)) {

			

			$this->session->set_flashdata('success', 'El registro se elimino con éxito.');

		} else {

			$this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');

		}

		redirect('admin/faq');

	}

         





}

