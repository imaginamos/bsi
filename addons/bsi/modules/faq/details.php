<?php



defined('BASEPATH') or exit('No direct script access allowed');



class Module_Faq extends Module {



    public $version = '1.3';



    public function info() {

        return array(

            'name' => array(

                'en' => 'FAQ',

                'es' => 'Preguntas Frecuentes'

            ),

            'description' => array(

                'en' => 'Preguntas Frecuentes © Juan Zapata',

                'en' => 'FAQ © Juan Zapata',

            ),

            'frontend' => TRUE,

            'backend' => TRUE,

            'menu' => 'content'

        );

    }



    public function install() {



        /* Creación del directorio para carga de imagenes */

        $this->dbforge->drop_table('faq');



        $faq = array(

            'id' => array(

                'type' => 'INT',

                'constraint' => '11',

                'auto_increment' => true

            ),

            'pregunta' => array(

                'type' => 'TEXT',

                'constraint' => '',

                'null' => false

            ),

             'respuesta' => array(

                'type' => 'TEXT',

                'constraint' => '',

                'null' => true

            ),
             'position' => array(

                 'type' => 'INT', 

                 'constraint' => '11', 

                 'null' => true),

                         );

              

     

        $this->dbforge->add_field($faq);

        $this->dbforge->add_key('id', true);



         if ($this->dbforge->create_table('faq') AND

                is_dir($this->upload_path . 'faq') OR @mkdir($this->upload_path . 'faq', 7777, TRUE)) {

           
             return TRUE;

        }

      

        }





    public function uninstall() {

        $this->dbforge->drop_table($this->faq);



      

        return true;

    }



    public function upgrade($old_version) {

        return true;

    }



    public function help() {

        return "Modulo de Nuestro Equipo";

    }



}



/* Fin del archivo details.php */