<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Widget_footer extends Widgets
{
    // The widget title,  this is displayed in the admin interface
    public $title = array(        
        'en' => 'footer(Widget)',
        'es' => 'pie(Widget)',
    );
    public $description = array(        
        'en' => 'footer.',
        'es' => 'pie.',
    );
 
    // The author's name
    public $author = '';
 
    // The authors website for the widget
    public $website = 'www.imaginamos.com';
 
    //current version of your widget
    public $version = '1.0';
	
    public function run()
    {
        $this->db->order_by("id", "desc"); 
        $this->db->limit(1);
    	$home_logo = $this->db->get($this->db->dbprefix.'footer')
        ->result_array();
    	return array('home_logo' => $home_logo);
    }
}