<div class="container">
    <div class="row mtop40">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h2 class="color-text-blue"><strong>Galeria</strong></h2>
            <p>{{ intro.text }}</p>
        </div>
    </div>
    <br>
    <div class="row">
        {{ gallery }}
            <div class="col-sm-6 col-md-3">
                <div class="thumbnail">
                    {{ if type == 1 }}
                        <a href="{{ content }}" target="_blanck">
                            <div style="overflow: hidden;max-height:202px;">
                                <small class="title-gallery">{{ title }}</small>
                                <img src="{{ content }}" alt="imagen" data-src="holder.js/300x200" class="img-responsive" style="min-width: 100%;">
                            </div>
                        </a>
                    {{ endif }}
                    {{ if type == 2 }}
                    <div style="overflow: auto;max-height:202px;">
                        {{ content }}
                    </div>
                    {{ endif }}
                </div>
            </div>
        {{ /gallery }}
    </div>
    {{ pagination }}
</div>
<div class="push"></div>


