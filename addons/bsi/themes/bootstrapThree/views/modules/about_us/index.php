<div class="row">
	<!-- TITULO -->
	<div class="col-md-12"><h3>{{ data:title }}</h3></div>
	<br>
</div>


<div class="row">
	<div class="col-sm-6 col-md-6">
		<div class="thumbnail">
			<!-- IMAGEN -->
			{{ if data:image }}
			<div style="overflow: hidden;max-height:490px;">
				<img src="{{ data:image }}" alt="imagen" data-src="holder.js/300x200" class="img-responsive" style="min-width: 100%;">
			</div>
			{{ endif }}
		</div>
	</div>
<!--
	<div class="col-sm-6 col-md-6">
		<div class="thumbnail">
			<!-- VIDEO 
			{{ if data:video }}
			<div class="video">
				{{ data:video }}
			</div>
			{{ endif }}
		</div>
	</div>-->

</div>

<hr>

<div class="row">
	<!-- TEXTO -->
	<div class="col-md-12">{{ data:text }}</div>
</div>