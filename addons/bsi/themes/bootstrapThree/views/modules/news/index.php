<div class="container">

	<!-- Modulo Titulo centrado -->
	<div class="row mtop40">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<h2 class="color-text-blue"><strong>Noticias/Publicaciones</strong></h2>
		</div>
	</div><!-- /END Modulo Titulo centrado -->
	<br>
	<div class="container-fluid">

		{{ news }}
		<div class="row">
			<div class="col-sm-4 col-md-4">
				<div class="thumbnail">
					<div style="overflow: hidden;max-height:250px;">
						<img src="{{ image }}" data-src="holder.js/300x200" width="100%" alt="" class="img-responsive">
					</div>
				</div>
			</div>
			<div class="col-sm-8 col-md-8">
				<div class="caption">
					<h3><a href="{{ urlDetail }}">{{ title }}</a></h3>
					<p>{{ introduction }}</p>
					<small class="small-float">Publicado: {{ date }}</small><br>
					<a class="btn btn-primary btn-sm" href="{{ urlDetail }}">Ver Mas</a>
				</div>
			</div>
		</div>
		<hr>
		{{ /news }}

		{{ pagination }}


	</div>
</div>
<div class="push"></div>
